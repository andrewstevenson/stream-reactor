# Stream Reactor
Streaming reference architecture built around Kafka. 

![Alt text](https://datamountaineer.files.wordpress.com/2016/01/stream-reactor-1.jpg?w=1320)

A collection of components to build a real time ingestion pipleline

#### [Kafka-Connect-Cassandra](kafka-connect/README.md)

Kafka connect Cassandra connector and sink to write Kafka topic payloads to Cassandra.

#### [Confluent Flume Kafka Channel](flume-ng/README.md)

Untested modified Flume Kafka channel for [Confluent](www.confluent.io) integration.
